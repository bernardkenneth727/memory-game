document.addEventListener('DOMContentLoaded', () => {
  //image options
  const imagesArray = [
    {
      name: 'MQ_100x100',
      img: 'images/MQ_100x100.png'  
    },
    {
      name: 'UniMelb_100x100',
      img: 'images/UniMelb_100x100.png' 
    },
    {
      name: 'unsw_100x100',
      img: 'images/unsw_100x100.png'  
    },
    {
      name: 'UOW_100x100',
      img: 'images/UOW_100x100.png'  
    },
    {
      name: 'usyd_100x100',
      img: 'images/usyd_100x100.png'  
    },
    {
      name: 'hotdog',
      img: 'images/UTS.png'
    },
    {
      name: 'MQ_100x100',
      img: 'images/MQ_100x100.png'
    },
    {
      name: 'UniMelb_100x100',
      img: 'images/UniMelb_100x100.png'
    },
    {
      name: 'unsw_100x100',
      img: 'images/unsw_100x100.png'
    },
    {
      name: 'UOW_100x100',
      img: 'images/UOW_100x100.png'
    },
    {
      name: 'usyd_100x100',
      img: 'images/usyd_100x100.png'
    },
    {
      name: 'hotdog',
      img: 'images/UTS.png'
    }
  ]

  imagesArray.sort(() => 0.75 - Math.random()) //

  const grid = document.querySelector('.grid')
  const resultDisplay = document.querySelector('#result')
  let imagesChosen = [] //
  let imagesChosenId = [] //
  let imagesWon = [] 

  //create board
  function setup() {
    for (let i = 0; i < imagesArray.length; i++) {
      const images = document.createElement('img')
      images.setAttribute('src', 'images/blank.png')
      images.setAttribute('data-id', i)
      images.addEventListener('click', flip)
      grid.appendChild(images) 
    }
  }

  //check matches
  function Match() {
    const images = document.querySelectorAll('img')
    const optionOneId = imagesChosenId[0]
    const optionTwoId = imagesChosenId[1]
    
    if(optionOneId == optionTwoId) {
      images[optionOneId].setAttribute('src', 'images/blank.png')
      images[optionTwoId].setAttribute('src', 'images/blank.png')
      alert('You have clicked the same image!')
    }
    else if (imagesChosen[0] === imagesChosen[1]) {
      alert('You found a match')
      images[optionOneId].setAttribute('src', 'images/white.png')
      images[optionTwoId].setAttribute('src', 'images/white.png')
      images[optionOneId].removeEventListener('click', flip)
      images[optionTwoId].removeEventListener('click', flip)
      imagesWon.push(imagesChosen)
    } else {
      images[optionOneId].setAttribute('src', 'images/blank.png')
      images[optionTwoId].setAttribute('src', 'images/blank.png')
      alert('Sorry, try again')
    }
    imagesChosen = []
    imagesChosenId = []
    resultDisplay.textContent = imagesWon.length
    if  (imagesWon.length === imagesArray.length/2) {
      resultDisplay.textContent = 'Congratulations! You matched them all!'
    }
  }

  //flip 
  function flip() {
    let cardId = this.getAttribute('data-id')
    imagesChosen.push(imagesArray[cardId].name)
    imagesChosenId.push(cardId)
    this.setAttribute('src', imagesArray[cardId].img)
    if (imagesChosen.length ===2) {
      setTimeout(Match, 500)
    }
  }

  setup()
})
